import React, { Component } from 'react';
import { StyleSheet, Platform } from 'react-native';
import { Router, Scene } from 'react-native-router-flux';
import Icon from 'react-native-vector-icons';

import Home from '../containers/Home';
import Login from '../containers/Login';
import SignUp from '../containers/SignUp';
import Chatbot from '../containers/Chatbot';
import Profile from '../containers/Profile';
import Favplaces from '../containers/Favplaces';
import Menu from '../containers/Sidemenu';
import Drawer1 from '../containers/drawer1';
import Locationinfo from '../containers/Locationinfo';


const MenuIcon=()=>{
  return(
    <Icon name="menu" size={30}/>
  )
}
class Navigation extends Component{
  render(){
    return (
      <Router>
        <Scene key="root">
          <Scene
            initial
            key="login"
            component={Login}
            hideNavBar={true}
            title="Login"
          />
          <Scene key="singup"
            component={SignUp}
            title="Sign Up"
          />
          <Scene key="chatbot"
            component={Chatbot}
            title="chatbot"
          />
          <Scene key="locationinfo"
            component={Locationinfo}
            title="Location Information"
          />
          <Scene key="profile"
            component={Profile}
            title="My Profile"
          />
          <Scene key="favplaces"
            component={Favplaces}
            title="My Favourite Places"
          />
          <Scene key="home"
            component={Home}
            title="Home"
            hideNavBar={true}
          />
  
        {/* <Scene initial key="drawer"
            drawer
            contentComponent={Menu}
            drawerIcon={MenuIcon}
            drawerWidth={300}
            hideNavBar>
            <Scene key="drawer1"
              component={Drawer1}
              title="page1"
            />
        </Scene> */}
        </Scene>
      </Router>
    );
  }
} 


export default Navigation;

import React, { Component } from 'react';
import { Actions } from 'react-native-router-flux';
import { Content,List,ListItem } from 'native-base';
import {
    StyleSheet,
    Text,
    View,
  } from 'react-native';
  import { firebaseRef } from "../../services/Firebase";

const logout=()=>{
    firebaseRef.auth().signOut()
  .then(function() {
    console.log("logged out");
    Actions.login();
  })
  .catch(function(error) {
    console.log(error);
  });
}
export default class Menu extends Component{
    render(){
        return(
            <View style={styles.container}>
                <View style={styles.header}>
                   


                </View>
                <View>
                    <Content>
                        <List>
                            <ListItem>
                                <Text onPress={() => Actions.profile()}>My Profile</Text>
                            </ListItem>
                            <ListItem>
                                <Text onPress={() => Actions.favplaces()}>Favourite Places</Text>
                            </ListItem>
                            <ListItem>
                                <Text onPress={() => Actions.chatbot()}>Chat Bot</Text>
                            </ListItem>
                            <ListItem>
                                <Text onPress={() => Actions.locationinfo()}>Location Info</Text>
                            </ListItem>
                            <ListItem>
                                <Text onPress={() => logout()}>Log Out</Text>
                            </ListItem>
                        </List>
                    </Content>
                </View>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
      flex: 1,
      justifyContent: 'center',
      alignItems: 'center',
      backgroundColor: '#ffffff',
      fontSize: 70,
    },
    welcome: {
      fontSize: 20,
      textAlign: 'center',
      margin: 10,
      color: 'black',
    },
    header: {
        fontSize: 20,
        textAlign: 'center',
        margin: 10,
        color: 'black',
        backgroundColor: '#34495e',
      },
  });

 
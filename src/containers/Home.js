import React, { Component } from 'react';
import {
  StyleSheet,
  Text,
  View,
  TouchableOpacity
} from 'react-native';
import { Actions } from 'react-native-router-flux';
import Drawer from "react-native-drawer";
import Menu from '../containers/Sidemenu';
import { Container,Content,Button,Icon, Header,Title } from 'native-base';
class Home extends Component{
  render(){
    return (
      <Drawer
          // open={true}
          ref={ref => (this._drawer = ref)}
          content={
            <Menu sidemenu={this._drawer}/>
          }
          type="overlay"
          tapToClose={true}
          openDrawerOffset={0.4}
          panCloseMask={0.5}
          closedDrawerOffset={-3}
          styles={drawerStyles}
          tweenHandler={ratio => ({
            //main: { opacity: (2 - ratio) / 2 }    //reduce opacity of main view
          })}
          captureGestures={true}
        >
        <View style={styles.main}
                style={{
                  width: 50
                }}
              >
                <TouchableOpacity
                  onPress={() => {
                    this._drawer.open(),
                      <Menu/>;
                  }}
                >
                  <Icon
                    name="ios-menu" // this is the menu icon
                    size={30}
                    style={{ margin: 15 }}
                  />
                  
                </TouchableOpacity>
            </View>

            <View style={styles.main}>
              <Text style={styles.cardText}>Welcome!!</Text>
              <Text style={styles.cardText}>Travel Guide App</Text>
            </View>
        {/* <Container>
        <Header>
          <Button onPress={() => this._drawer.open()}>
            <Icon name="ios-menu"></Icon>
          </Button>
          <Title>Home</Title>
        </Header>
        <Content style={styles.container}>
          
        </Content>
        </Container> */}
      </Drawer>
    );
  }
}

const styles = StyleSheet.create({
  main: {
    flex: 1,
    padding: 30,
    marginTop: 0,
    flexDirection: 'column',
    justifyContent: 'center',
    backgroundColor: '#000000'
  },
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#ffffff',
  },
  welcome: {
    fontSize: 20,
    textAlign: 'center',
    margin: 10,
    color: 'black',
  },
  cardText: {
    fontSize: 30,
    color: 'blue',
    alignSelf: 'center'
  },
});

const drawerStyles = {
  drawer: { shadowColor: "#000000", shadowOpacity: 0.8, shadowRadius: 3 },
  main: { paddingLeft: 3 }
};


export default Home;
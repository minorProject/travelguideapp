import React, { Component } from 'react';
import {
  StyleSheet,
  Text,
  View,
  Alert,
  ToastAndroid,
  Picker,
  KeyboardAvoidingView
} from 'react-native';
import { Actions } from 'react-native-router-flux';
import { Container,Form,Item,Label,Input,Button,DatePicker, Content  } from 'native-base';
import { firebaseRef } from "../../services/Firebase";
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';
// import DatePicker from 'react-native-datepicker';

class SignUp extends Component {
    constructor(props) {
        super(props);
        this.state = {
          email: "",
          firstName:"",
          lastName:"",
          phno:"+91",
          password1:"",
          password:"",
          birthday: "",
          gender: "",
        };
      }
    

    signUpUser(){
        const{email,password,firstName,lastName,phno,birthday,gender}=this.state;
        try{
            if(phno.length<10){
            alert("Enter a valid phone number!");
            }
            if(this.state.password.length>=6 && this.state.email!="" && this.state.password!=""){
                firebaseRef.auth().createUserWithEmailAndPassword(email,password).catch(function(error){
                    console.log('error registering user');
                    alert("Email address exists.");
                }).then((user)=>{
                    console.log("User registered")
                    console.log(user)
                    if(user!=undefined){
                        var reguser=firebaseRef.auth().currentUser;
                        console.log(reguser)
                        if (reguser != null) {
                            firebaseRef.database().ref('/users/' + reguser.uid).set({
                                name: firstName+" "+lastName,
                                email: email,
                                phoneNumber: phno,
                                profile_picture: "",
                                birthday: birthday,
                                gender: gender,
                            });
                            Actions.home();
                        }else{
                            alert("Error in Registering your details!")
                        }
                    }else{
                        console.log('user is not defined');
                    }
                });
                
            }else if(this.state.password.length<6){
                alert("password should contain atleast 6 chars!");
                return;
                
            }else{
                alert("Input valid details.!!");
            }

        }catch(error){
            console.log(error);
        }
    }

    render (){
        return(
        <Container style={styles.main}>
        <KeyboardAwareScrollView>
            <Form>
                <Item floatingLabel>
                    <Label>First Name</Label>
                    <Input autoCorrect={false} onChangeText={(firstName) => this.setState({firstName})}></Input>
                </Item>
                <Item floatingLabel>
                    <Label>Last Name</Label>
                    <Input autoCorrect={false} onChangeText={(lastName) => this.setState({lastName})}></Input>
                </Item>
                 {/* <Item>
                    <DatePicker
                                style={{ width: 210 }}
                                birthday={this.state.birthday}
                                mode="date"
                                placeHolderText={this.state.birthday.toString()}
                                format="YYYY-MM-DD"
                                //format="DD-MM-YYYY"
                                //minDate="2016-05-01"
                                //maxDate= "2018-1-19"
                                //maxDate="new Data()"
                                confirmBtnText="Confirm"
                                cancelBtnText="Cancel"
                                customStyles={{
                                    dateInput: {
                                        marginRight: -5
                                    }
                                    
                                }}
                                onDateChange={(birthday) => { this.setState({birthday}) }}

                    />
                <Label>
                    {this.state.birthday.toString()}
                </Label> 
                </Item>  */}
                <Item>
                <Picker
                    selectedValue={this.state.gender}
                    style={{ height: 50, width: 200 }}
                    onValueChange={(gender) => this.setState({gender})}>
                    <Picker.Item label="Male" value="Male" />
                    <Picker.Item label="Female" value="Female" />
                    <Picker.Item label="Other" value="Other" />
                </Picker>
                </Item>
                <Item floatingLabel>
                    <Label>Email</Label>
                    <Input autoCorrect={false} autoCapitalize='none' onChangeText={(email) => this.setState({email})}></Input>
                </Item>
                <Item floatingLabel>
                    <Label>Phone Number(+91)</Label>
                    <Input autoCorrect={false} onChangeText={(phno) => this.setState({phno})}></Input>
                </Item>
                <Item floatingLabel>
                    <Label>Password</Label>
                    <Input secureTextEntry={true} autoCorrect={false} onChangeText={(password) => this.setState({password})}></Input>
                </Item>
                <Item floatingLabel>
                    <Label>Re-enter Password</Label>
                    <Input secureTextEntry={true} autoCorrect={false} onChangeText={(password1) => this.setState({password1})}></Input>
                </Item>
            <Button style={{marginTop:20}} block rounded onPress={() => this.signUpUser()}>
                <Text>Sign In</Text>
            </Button>
            </Form>
        </KeyboardAwareScrollView>
        </Container>
    );
  }
}

const styles = StyleSheet.create({
    main: {
      flex: 1,
      padding: 30,
      marginTop: 0,
      flexDirection: 'column',
      justifyContent: 'flex-start',
      backgroundColor: '#0000ff'
    },
    title: {
      marginBottom: 20,
      fontSize: 25,
      textAlign: 'center'
    },
    searchInput: {
      height: 50,
      padding: 4,
      marginRight: 5,
      marginTop: 5,
      fontSize: 23,
      borderWidth: 1,
      borderColor: 'white',
      borderRadius: 8,
      color: 'white'
    },
    buttonText: {
      fontSize: 18,
      color: '#111',
      alignSelf: 'center'
    },
    cardText: {
        fontSize: 30,
        color: 'blue',
        alignSelf: 'center'
      },
    button: {
      height: 45,
      flexDirection: 'row',
      backgroundColor:'white',
      borderColor: 'white',
      borderWidth: 1,
      borderRadius: 8,
      marginBottom: 10,
      marginTop: 10,
      alignSelf: 'stretch',
      justifyContent: 'center'
    }
  });

// const styles = StyleSheet.create({
//   container: {
//     flex: 1,
//     justifyContent: 'center',
//     backgroundColor: 'white',
//   },
//   welcome: {
//     fontSize: 20,
//     textAlign: 'center',
//     margin: 10,
//     color: 'black',
//   },
// });

export default SignUp;
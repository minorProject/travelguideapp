import React, { Component } from 'react';
import {
  StyleSheet,
  Text,
  View,
  
} from 'react-native';
import { Actions } from 'react-native-router-flux';
import { Container,Form,Item,Label,Input,Button } from 'native-base';
import { firebaseRef } from "../../services/Firebase";

class Login extends Component {
    constructor(props) {
        super(props);
        this.state = {
          password: "",
          email: "",
        };
      }
    

    login = (email,password) =>{
        try{
            if(this.state.email!="" && this.state.password!=""){
                firebaseRef.auth().signInWithEmailAndPassword(email,password).then(function (user){
                    console.log("logged in");
                    if(user != undefined){
                        Actions.home();
                    }
                })
            }
            else{
                alert("Check the email and password!");
                return;
            }

        }catch(error){
            console.log(error);
        }
    }

    render (){
        return(
        <Container style={styles.main}>
            <Form>
                <Item floatingLabel>
                    <Label>Email</Label>
                    <Input autoCorrect={false} autoCapitalize='none' onChangeText={(email) => this.setState({email})}></Input>
                </Item>
                <Item floatingLabel>
                    <Label>Password</Label>
                    <Input secureTextEntry={true} autoCorrect={false} onChangeText={(password) => this.setState({password})}></Input>
                </Item>
            <Button style={{marginTop:20}} block rounded onPress={() => this.login(this.state.email,this.state.password)}>
                <Text>Log In</Text>
            </Button>
            <Button style={{marginTop:20}} block rounded onPress={() => Actions.singup()}>
                <Text>Create an Acoount</Text>
            </Button>
            </Form>
        </Container>
    );
  }
}
const styles = StyleSheet.create({
    main: {
      flex: 1,
      padding: 30,
      marginTop: 0,
      flexDirection: 'column',
      justifyContent: 'center',
      backgroundColor: '#808080'    //#2a8ab7
    },
    title: {
      marginBottom: 20,
      fontSize: 25,
      textAlign: 'center'
    },
    searchInput: {
      height: 50,
      padding: 4,
      marginRight: 5,
      marginTop: 5,
      fontSize: 23,
      borderWidth: 1,
      borderColor: 'white',
      borderRadius: 8,
      color: 'white'
    },
    buttonText: {
      fontSize: 18,
      color: '#111',
      alignSelf: 'center'
    },
    cardText: {
        fontSize: 30,
        color: 'blue',
        alignSelf: 'center'
      },
    button: {
      height: 45,
      flexDirection: 'row',
      backgroundColor:'white',
      borderColor: 'white',
      borderWidth: 1,
      borderRadius: 8,
      marginBottom: 10,
      marginTop: 10,
      alignSelf: 'stretch',
      justifyContent: 'center'
    }
  });


// const styles = StyleSheet.create({
//   container: {
//     flex: 1,
//     justifyContent: 'center',
//     backgroundColor: 'white',
//   },
//   welcome: {
//     fontSize: 20,
//     textAlign: 'center',
//     margin: 10,
//     color: 'black',
//   },
// });


export default Login;
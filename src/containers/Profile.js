import React, { Component } from 'react';
import {
  StyleSheet,
  Text,
  View
} from 'react-native';
import { Button,Card, CardItem,Body } from 'native-base';
import { firebaseRef } from "../../services/Firebase";

// var user = firebaseRef.auth().currentUser;
// console.log(user.uid);


class Profile extends Component{
    constructor(props) {
        super(props);
        this.state = {
            name:"Imedya",
            email: "imedyaosandi@gmail.com",
        };
      }
    componentDidMount(){
      firebaseRef.auth().onAuthStateChanged(function(user) {
        if (user) {
           console.log(user.uid);
           this.setState({
            name:user.uid.name,
            email: user.email,
          });
   } else {
     console.log("no user");
   }
 });
}
    render(){
        return(
            <View  style={styles.main}>
                <Card>
                    <CardItem header bordered>
                    <Text style={styles.cardText}>My Profile</Text>
                    </CardItem>
                    <CardItem>
                    <Body>
                    <Text style={styles.buttonText}>Name : {this.state.name}</Text>
                    <Text style={styles.buttonText}>Email : {this.state.email}</Text>
                    </Body>
                    </CardItem>
                </Card>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    main: {
      flex: 1,
      padding: 30,
      marginTop: 0,
      flexDirection: 'column',
      justifyContent: 'flex-start',
      backgroundColor: '#0000ff'
    },
    title: {
      marginBottom: 20,
      fontSize: 25,
      textAlign: 'center'
    },
    searchInput: {
      height: 50,
      padding: 4,
      marginRight: 5,
      marginTop: 5,
      fontSize: 23,
      borderWidth: 1,
      borderColor: 'white',
      borderRadius: 8,
      color: 'white'
    },
    buttonText: {
      fontSize: 18,
      color: '#111',
      alignSelf: 'stretch'
    },
    cardText: {
        fontSize:30,
        color: 'blue',
        alignSelf: 'center'
      },
    button: {
      height: 45,
      flexDirection: 'row',
      backgroundColor:'white',
      borderColor: 'white',
      borderWidth: 1,
      borderRadius: 8,
      marginBottom: 10,
      marginTop: 10,
      alignSelf: 'stretch',
      justifyContent: 'center'
    }
  });
  
  export default Profile;
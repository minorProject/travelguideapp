import React, { Component } from 'react';
import {
    StyleSheet,
    Text,
    View,
    TextInput,
  } from 'react-native';

  
class Chatbot extends Component{
  constructor(props) {
    super(props);
    this.state = {
      reply:"",
      usermsg: "",
    };
  }
  componentDidMount() {
    this.fetchMsg("")
    this.fetchMsg(this.state.usermsg);
    
  }

  fetchMsg(usermsg) {
    fetch(
      `http://192.168.43.211:5000/bot/${usermsg}`
    )
      .then(res => res.json())
      .then(json => {
      console.log(json);
      
      //  this.setState({
      //   reply:json.name,
      // });
      });
  }
    render(){
      const usermsg=this.state.usermsg;
        return(
            <View style={styles.container}>
               <TextInput
                  style={styles.nameInput}
                  placeHolder="Write your Msg"
                  value={this.state.usermsg}
                  onChangeText={(password) => this.setState({password})}
                  onSubmitEditing={() => this.fetchMsg(this.state.usermsg)}
                >
            </TextInput>
            </View>
        );
    }
}


  const styles = StyleSheet.create({
    nameInput: {
      height:24*2,
      margin:24,
      paddingHorizontal:24,borderColor: '#111111',
      borderWidth:1,
  },
    welcome: {
      fontSize: 20,
      textAlign: 'center',
      margin: 10,
      color: 'black',
    },
  });
  
  export default Chatbot;
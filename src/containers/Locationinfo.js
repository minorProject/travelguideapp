import React, { Component } from 'react';
import {
  View,
  Text,
  StyleSheet,
  TextInput,
  TouchableHighlight
} from 'react-native';
import { API_KEY } from "../../services/openweather";
import { Button,Card, CardItem,Body } from 'native-base';
import Weather from './weather';


// weather = (city) =>{
//     return fetch('https://facebook.github.io/react-native/movies.json')
//     .then((response) => response.json())
//     .then((responseJson) => {
//       return responseJson.movies;
//     })
//     .catch((error) => {
//       console.error(error);
//     });
// }


class Locationinfo extends Component{
    constructor(props) {
        super(props);
        this.state = {
            city:null,
            isLoading: false,
            temperature: 0,
            weatherCondition: null,
            error: null,
            date:null,
        };
      }

    componentDidMount() {
        navigator.geolocation.getCurrentPosition(
          position => {
            this.fetchWeather(position.coords.latitude, position.coords.longitude);
          },
          error => {
            this.setState({
              error: 'Error Gettig Weather Condtions'
            });
          }
        );
      }
    
      fetchWeather(lat = 25, lon = 25) {
        fetch(
          `http://api.openweathermap.org/data/2.5/weather?lat=${lat}&lon=${lon}&APPID=${API_KEY}&units=metric`
        )
          .then(res => res.json())
          .then(json => {
          console.log(json);
          console.log(json.weather[0].main);
           this.setState({
            city:json.name,
            temperature: json.main.temp,
            weatherCondition: json.weather[0].main,
            isLoading: false
          });
          });
      }
    weather(city){
    const searchtext="select item.condition from weather.forecast where woeid in (select woeid from geo.places(1) where text='" + city + "') and u='c'"
    // fetch("https://query.yahooapis.com/v1/public/yql?q=select%20*%20from%20weather.forecast%20where%20woeid%20in%20(select%20woeid%20from%20geo.places(1)%20where%20text%3D%22nome%2C%20ak%22)&format=json&env=store%3A%2F%2Fdatatables.org%2Falltableswithkeys")
    fetch("https://query.yahooapis.com/v1/public/yql?q=" + searchtext + "&format=json")
    .then((response) => response.json())
    .then((responseJson) => {
      console.log(responseJson);
      this.setState({
        temperature: responseJson.query.results.channel.item.condition.temp,
        weatherCondition: responseJson.query.results.channel.item.condition.text,
        date:responseJson.query.results.channel.item.condition.date,
        isLoading: false
      });
    })
    .catch((error) => {
      console.error(error);
    });
}
    
    render() {
        const { isLoading,temperature,weatherCondition,city,date } = this.state;
        return (
          <View style={styles.main}>
            <TextInput
                  style={styles.searchInput}
                  onChangeText={(city) => this.setState({city})}
                >{this.state.city}
            </TextInput>
            <Button 
                    style = {styles.button}
                    underlayColor= "white"
                    onPress={() =>this.weather(this.state.city)}
                  >
                <Text
                style={styles.buttonText}>
                SEARCH
                </Text>
            </Button>
            
                <Card>
                    <CardItem header bordered>
                    <Text style={styles.cardText}>{city}</Text>
                    </CardItem>
                    <CardItem>
                    <Body>
                    <Text style={styles.buttonText}>Date:{date}</Text>
                    <Text style={styles.buttonText}>Temperature: {temperature}˚C </Text>
                    <Text style={styles.buttonText}>Weather Condition:{weatherCondition}</Text>
                    
                    {/* <Weather weather={weatherCondition} temperature={temperature} /> */}
                    </Body>
                    </CardItem>
                </Card>
        
           </View>
        )
      }
    }

    const styles = StyleSheet.create({
        main: {
          flex: 1,
          padding: 30,
          marginTop: 0,
          flexDirection: 'column',
          justifyContent: 'flex-start',
          backgroundColor: '#0000ff'
        },
        title: {
          marginBottom: 20,
          fontSize: 25,
          textAlign: 'center'
        },
        searchInput: {
          height: 50,
          padding: 4,
          marginRight: 5,
          marginTop: 5,
          fontSize: 23,
          borderWidth: 1,
          borderColor: 'white',
          borderRadius: 8,
          color: 'white'
        },
        buttonText: {
          fontSize: 18,
          color: '#111',
          alignSelf: 'center'
        },
        cardText: {
            fontSize: 30,
            color: 'blue',
            alignSelf: 'center'
          },
        button: {
          height: 45,
          flexDirection: 'row',
          backgroundColor:'white',
          borderColor: 'white',
          borderWidth: 1,
          borderRadius: 8,
          marginBottom: 10,
          marginTop: 10,
          alignSelf: 'stretch',
          justifyContent: 'center'
        }
      });



//   const styles = StyleSheet.create({
//     container: {
//       flex: 1,
//       justifyContent: 'center',
//       alignItems: 'center',
//       backgroundColor: '#ffffff',
//     },
//     welcome: {
//       fontSize: 20,
//       textAlign: 'center',
//       margin: 10,
//       color: 'black',
//     },
//   });
  
  export default Locationinfo;